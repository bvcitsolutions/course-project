import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RecipeService } from '../recipes/recipe.service';
import { Recipe } from '../recipes/recipe.model';

import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataStorageService {

  constructor(private http: HttpClient, private rpService: RecipeService) { }

  fetchRecipes() {
    return this.http.get<Recipe[]>('https://angular-kurs-22da4.firebaseio.com/recipes.json')
      .pipe(
        tap(recipes => {
          this.rpService.setRecipes(recipes);
        })
      );
  }

  storeRecipes() {
    const recipes = this.rpService.getRecipes();
    this.http.put('https://angular-kurs-22da4.firebaseio.com/recipes.json', recipes).subscribe(response => { console.log(response) });
  }
}
