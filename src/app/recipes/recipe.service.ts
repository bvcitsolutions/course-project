import { Injectable, EventEmitter } from '@angular/core';
import { Recipe } from './recipe.model';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from '../shopping-list/shopping-list.service';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {

  recipeChanged = new Subject<Recipe[]>();

  private recipes: Recipe[] = [
    // tslint:disable-next-line: max-line-length
    new Recipe('Test recept',
      'Ovo je test',
      'https://coolinarika.azureedge.net/images/_variations/5/9/59b32ca86bcd8ce3f4465477f594096c_header.jpg?v=1',
      [
        new Ingredient('Prvi sastojak', 1),
        new Ingredient('Drugi sastojak', 13)
      ]),
    // tslint:disable-next-line: max-line-length
    new Recipe('Test recept 2',
      'Ovo je test 2',
      'https://coolinarika.azureedge.net/images/_variations/5/9/59b32ca86bcd8ce3f4465477f594096c_header.jpg?v=1',
      [
        new Ingredient('Treci sastojak', 5),
        new Ingredient('Cetvrti sastojak', 34)
      ])
  ];

  constructor(private slService: ShoppingListService) { }

  setRecipes(recipes: Recipe[]) {
    this.recipes = recipes;
    this.recipeChanged.next(this.recipes.slice());
  }

  getRecipes() {
    return this.recipes.slice();
  }

  getRecipe(index: number) {
    return this.recipes[index];
  }

  addIngredientsToShoppingList(ingredients: Ingredient[]) {
    this.slService.addIngredients(ingredients);
  }

  addRecipe(recipe: Recipe) {
    this.recipes.push(recipe);
    this.recipeChanged.next(this.recipes.slice());
  }

  updateRecipe(index: number, recipe: Recipe) {
    this.recipes[index] = recipe;
    this.recipeChanged.next(this.recipes.slice());
  }

  deleteRecipe(index: number) {
    this.recipes.splice(index, 1);
    this.recipeChanged.next(this.recipes.slice());
  }

}
