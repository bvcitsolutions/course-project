import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Recipe } from './recipe.model';
import { DataStorageService } from '../shared/data-storage.service';

@Injectable({
  providedIn: 'root'
})
export class RecipeResolverService implements Resolve<Recipe[]> {
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.dsService.fetchRecipes();
  }

  constructor(private dsService: DataStorageService) { }
}
